def insertion_sort(numbers):
    for i in range(1, len(numbers)):
        key = numbers[i]
        j = i-1
        while j >= 0 and key < numbers[j] :
                numbers[j + 1] = numbers[j]
                j -= 1
        numbers[j + 1] = key

numbers = list(map(int, input("Enter integer number with space: ")))
sorted_numbers = insertion_sort(numbers)     
print("Sorted number is", sorted_numbers)