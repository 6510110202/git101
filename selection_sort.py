def selection_sort(numbers):
    for i in range(len(numbers)):
        min_idx = i
        for j in range(i+1, len(numbers)):
            if numbers[min_idx] > numbers[j]:
                min_idx = j
        numbers[i], numbers[min_idx] = numbers[min_idx], numbers[i]

numbers = list(map(int, input("Enter integer number with space: ")))
sorted_numbers = selection_sort(numbers)     
print("Sorted number is", sorted_numbers)
